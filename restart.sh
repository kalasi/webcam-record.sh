echo "Killing old process."

pid=$(ps -aux | grep ffmpeg | grep -v grep | awk '{print $2}')
kill "$pid"

echo "Starting new process in the background."
./webcamr_start.sh &

echo "rsyncing video to server."
./rsync.sh
