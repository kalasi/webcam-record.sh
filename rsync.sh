. ./config

echo "Syncing video files"

creds="${RSYNC_USERNAME}@${RSYNC_SERVER}"
rsync -avhPt --ignore-existing "${VIDEO_FOLDER}" "${creds}:${RSYNC_OUTPUT_VIDEO}"
