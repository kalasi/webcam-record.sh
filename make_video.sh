#!/usr/bin/env bash
# ISC License (ISC)
#
# Copyright (c) 2016, Austin Hellyer <hello@austinhellyer.me>
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
# SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER
# RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
# CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
# CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
#
# Originally by zeyla on GitHub.

# Source the config
. ./config

dir="${SCREENSHOT_FOLDER}/$1"

# If a screenshots directory argument is not given, exit
if [[ "$1" == "" ]]; then
    echo "Directory argument must be given."

    exit 1
fi

# If the daily screenshots directory doesn't exist, exit
if [[ ! -d "$dir" ]]; then
    echo "Directory: ${dir} does not exist"

    exit 1
fi

# Check if the file count in the directory is 0. If so, it's not valid. Maybe
# attempting to convert the next day's?
file_count=`ls -l "$dir" | wc -l`

if [[ $file_count -eq 0 ]]; then
    echo "File count is 0 in dir: ${dir}"

    exit 1
fi

# Make the videos directory if it doesn't exist
if [[ ! -d "${VIDEO_FOLDER}" ]]; then
    mkdir "${VIDEO_FOLDER}"
fi

# Path to the output video, named like './videos/2012-12-21.webm'
path="${VIDEO_FOLDER}/$1.webm"

# Remove it if it already exists
if [[ -f "$path" ]]; then
    rm "$path"
fi

echo "Converting screenshots to a video..."

ffmpeg -framerate 12 -pattern_type glob -i "${SCREENSHOT_FOLDER}/$1/*.jpeg" "$path" > /dev/null

echo "Syncing videos and screenshots..."
rsync -avhPt --ignore-existing "${VIDEO_FOLDER}" "${RSYNC_USERNAME}"@"${RSYNC_SERVER}":"${RSYNC_OUTPUT_VIDEOS}"
rsync -avhPt --ignore-existing "${SCREENSHOT_FOLDER}" "${RSYNC_USERNAME}"@"${RSYNC_SERVER}":"${RSYNC_OUTPUT_SCREENSHOTS}"
