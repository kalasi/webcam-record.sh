[![license-badge][]][license]

# webcam-record.sh

Record a video with a configured FPS for the day, stop it at the end of the day,
rsync it to a server, and then start recording again.

### Installation

1. `git clone https://gitlab.com/kalasi/webcam-record.sh`
2. `cd webcam-record.sh`
3. `chmod +x ./restart.sh ./rsync.sh ./webcamr_start.sh`
4. add a daily entry to your cronjob:
   `0 * * * * cd PATH_TO_PROJECT && ./restart.sh`

### Using It

- `./start.sh` to start recording;
- `./restart.sh` to stop other recording processes, start a new one, and rsync
  videos to a server;
- `./rsync.sh` to rsync videos to a server.

### Configuration

All configuration options are available in the `./config` file. There are no
flags to be passed to `./start.sh`.

### License

ISC. Full info in [LICENSE.md].


[LICENSE.md]: https://gitlab.com/kalasi/webcam-record.sh/blob/master/LICENSE.md
[license-badge]: https://img.shields.io/badge/license-ISC-blue.svg?style=flat-square
[license]: https://opensource.org/licenses/ISC
